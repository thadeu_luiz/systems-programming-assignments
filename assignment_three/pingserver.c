//==============================================================================
//
//       Filename:  pingserver.c
//
//    Description:  Simple ping server
//
//        Version:  1.0
//        Created:  09/07/2015 05:40:13 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define PORT "10666"
#define MAX_INCOMING_CONNECTIONS 10
#define MAX_PACK_LENGTH 1024*1024
#define N_MISS

int main(int argc, char* argv[]){
  int status, listener;
  struct addrinfo hint, *hostinfo, *attempt;

  if(argc != 1){
    printf("%s: Error - this program takes no args.\n", argv[0]);
    exit(1);
  }

  //prepare getaddrinfo for ipv6 addresses...
  //if no ipv6 address is found, get ipv4 addresses mapped to ipv6
  memset(&hint, 0, sizeof(hint));
  hint.ai_family = AF_INET6;
  hint.ai_socktype = SOCK_DGRAM;
  hint.ai_flags = AI_PASSIVE | AI_V4MAPPED;

  if( (status = getaddrinfo(NULL, PORT, &hint, &hostinfo)) ){
    fprintf(stderr, "Error getting address info: %s.\n", gai_strerror(status));
    exit(1);
  }

  //create socket
  for(listener = -1, attempt = hostinfo; attempt != NULL; attempt = attempt->ai_next){
    listener = socket(attempt->ai_family, attempt->ai_socktype, attempt->ai_protocol);
    if(listener != -1) break;
  }

  //no socket found...
  if(attempt == NULL){
    fprintf(stderr, "Error creating socket.\n");
    exit(1);
  }

  //bind our first created socket...
  if(bind(listener, attempt->ai_addr, attempt->ai_addrlen) == -1){
    status = errno;
    fprintf(stderr, "Error binding socket to port %s.\n errno %d.\n", PORT, status);
    exit(1);
  }
 
  //listener socket aquired and bound
  //we are free to deallocate our linked list
  freeaddrinfo(hostinfo);

#ifndef N_MISS
  srand(time(NULL));
#endif

  while(1){
    struct sockaddr_storage incoming_address;
    char buff[MAX_PACK_LENGTH];
    int read, send;
    socklen_t addr_len;

    addr_len = sizeof(incoming_address);
    memset(&incoming_address, 0, addr_len);
    memset(buff, 0, sizeof(buff));
    read = recvfrom(listener, buff, MAX_PACK_LENGTH, 0, (struct sockaddr*)&incoming_address, &addr_len);
    if(read == -1){
      status = errno;
      printf("Error reading from socket.\nerrno: %d.\n", status);
      continue;
    }
    printf("Read %d bytes from the socket:\n", read);
    fwrite(buff, sizeof(char), read, stdout);

#ifndef N_MISS
    usleep(rand()%1100000);
#endif

    send = sendto(listener, buff, read, 0, (struct sockaddr*)&incoming_address, addr_len);
    if(send < read){
      printf("Failed to send echo message to client.\n");
    }
  }

  return 0;
}
