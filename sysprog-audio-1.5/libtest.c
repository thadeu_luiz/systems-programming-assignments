//==============================================================================
//
//       Filename:  types.c
//
//    Description:  implementation of functions operating on types
//
//        Version:  1.0
//        Created:  10/14/2015 04:42:16 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>

#include "libtest.h"

int sendBlock(int socket, sockstorage_t target, socklen_t targetlen,
              void* data, int nbytes, int* seqn){
  int sent;
  Packet packet;
  packet.seqn = *seqn;
  memcpy(packet.data, data, nbytes);
  sent = sendto(socket, &packet, (PACK_HEADER_SIZE + nbytes), 0,
               (struct sockaddr*)&target, targetlen );
  do {
    if(sent == -1 || sent < PACK_HEADER_SIZE ){
      printf("Failed to send package or message header. aborting.\nerrno: %d", errno);
      exit(1);
    }
    //something got sent. increase seqn...
    (*seqn)++;

    //all bytes sent
    if(sent == PACK_HEADER_SIZE + nbytes)
      return 0;

    int delta = sent - PACK_HEADER_SIZE;
    nbytes -= delta;
    memmove(packet.data, packet.data + delta, nbytes);
    sent = sendto(socket, &packet, (PACK_HEADER_SIZE + nbytes), 0,
                 (struct sockaddr*)&target, targetlen );
  }while(1);

  return 0;
}

int readBuffer(int socket, Buffer* out){//out owns the data field...
  sockstorage_t sender;
  socklen_t senderlen = sizeof(sender);
  Packet packet;
  int read;
  read = recvfrom(socket, &packet, sizeof(packet), 0, (struct sockaddr*)&sender, &senderlen);

  if(read == -1){
    printf("Error reading from socket. Aborting.\n");
    exit(2);
  }

  //without a header, the packet is meaningless...
  if(read < PACK_HEADER_SIZE){
    return -1;
  }

  out->seqn = packet.seqn;
  out->size = read - PACK_HEADER_SIZE;
  if(out->size == 0){
    out->data = NULL;
    return 0;
  }
  out->data = malloc(out->size);
  if(out->data == NULL){
    printf("Error allocating data for buffer. Aborting.\n");
    exit(3);
  }
  memcpy(out->data, packet.data, out->size);
  return 0;
}

void freeBuffer(Buffer* buf){
  if(buf->data)
    free(buf->data);
  buf->data = NULL;
}

BufferQueueNode* newBufferQueueNode(){
  BufferQueueNode* node = calloc(1, sizeof(BufferQueueNode));
  if(node == NULL){
    printf("Out of memory creating node. Exiting.\n");
    exit(4);
  }
  return node;
}

void freeBufferQueueNode(BufferQueueNode* node){
  if(node == NULL)
    return;
  freeBuffer(&node->buffer);
  free(node);
}

BufferQueueNode* insertOnQueue(Buffer* buf, BufferQueue* queue){
  BufferQueueNode* node = newBufferQueueNode();
  node->buffer = *buf;
  buf->data = NULL;//transfered ownership of data...

  while(1){
    if(*queue == NULL){
      *queue = node;
      break;
    }
    if(node->buffer.seqn < (*queue)->buffer.seqn){
      node->next = *queue;
      *queue = node;
      break;
    }
    queue = &(*queue)->next;
  }
  return node;
}

void popFromQueue(BufferQueue* queue, Buffer* buf){
  //assume that queue is not empty
  BufferQueueNode* head = (*queue);
  *buf = head->buffer;
  head->buffer.data = NULL; //transferred ownership to buf
  *queue = head->next;
  freeBufferQueueNode(head);
}
