the protocol is based on a sequence numbercontained at the start of data packet.
the first packet sent by the client is part of the handshake protocol, it contains the name of the file to be sent.
the response of the server is a struct containing 3 uint32_t's, containing the audio file info. In particular, sample rate, sample depht and channel count.

the server then sends the audio file information and proceeds to forward the audio data to the client as follows
a sequence counter(unsigned long) is sent, to handle out-of-order arrival.
the binary payload.

if a message is truncated before the primary header is sent, the packet count wont change and the message will be resent.

the server will output packets at 150% speed. the client should have an unbounded buffer to store packets and playback them in order according to the sequence number, and should be enough time for the client to request a possibly lost packet(future implementation).

three filters were implemented, all server-sided.

stereo to mono:
  the right channel is ignored, only half the file samples are sent, so data usage is halved.
  usage: 'mono'

clip:
  applies a non linear transformation to the signal, once a sample becomes greater than X% of the maximum possible value, it gets flattened to X%.
  usage: 'c <percentage of clip>'

volume:
  linear scaling of the sample.
  usage: 'v <percentage of original value>'
