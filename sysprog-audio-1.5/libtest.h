//==============================================================================
//
//       Filename:  types.h
//
//    Description:  definition of types for the client-server comunication...
//
//        Version:  1.0
//        Created:  10/14/2015 03:02:39 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <stdint.h>
#include <netdb.h>

#ifndef  libtest_HPP
#define  libtest_HPP

typedef struct WavInfo {
  int sample_rate;
  int sample_size;
  int channels;
} WavInfo;

typedef struct Buffer {
  uint32_t seqn;
  uint32_t size;
  void* data;
} Buffer;

#define MAX_PACKET_PAYLOAD 1024
#define MAX_BUF_SIZE MAX_PACKET_PAYLOAD

#define MAX_FILENAME 1024

//basically, a serialized version of the Buffer...
#define PACK_HEADER_SIZE (int)sizeof(uint32_t)
typedef struct Packet {
  uint32_t seqn;
  char data[MAX_PACKET_PAYLOAD];
} Packet;


typedef struct sockaddr_storage sockstorage_t;

//this function sends a block of data entirely, in one or more send() calls, to
//a unconnected udp socket...
int sendBlock(int socket, sockstorage_t target, socklen_t targetlen,
              void* data, int nbytes, int* seqn);

//this function returns a filled buffer structure after reading from a
//udp socket...
int readBuffer(int socket, Buffer* out);//out owns the data field...

//releases memory aquired by a buffer...
void freeBuffer(Buffer* buf);

typedef struct BufferQueueNode {
  Buffer buffer;
  struct BufferQueueNode* next;
} BufferQueueNode;

BufferQueueNode* newBufferQueueNode();

void freeBufferQueueNode(BufferQueueNode*);

typedef BufferQueueNode* BufferQueue;

BufferQueueNode* insertOnQueue(Buffer*, BufferQueue*); //does ordered insertion on the queue
void popFromQueue(BufferQueue*, Buffer*);

#endif   // ----- #ifndef libtest_HPP  -----

