//==============================================================================
//
//       Filename:  audioclient.c
//
//    Description:  client...
//
//        Version:  1.0
//        Created:  10/15/2015 03:51:10 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "audio.h"
#include "libtest.h"

#define PORT "10666"

int main(int argc, char* argv[]){
  struct addrinfo hint, *target_info;
  int status, sock;

  if(argc < 3){
    printf("Usage: %s <hostname> <file> {filters}\n", argv[0]);
    exit(1);
  }

  memset(&hint, 0, sizeof(hint));
  hint.ai_family = AF_UNSPEC;
  hint.ai_socktype = SOCK_DGRAM;

  if(getaddrinfo(argv[1], PORT, &hint, &target_info) != 0){
    status = errno;
    fprintf(stderr, "Error retrieving getting host info.\nerrno: %d.\n", status);
    exit(1);
  }

  struct addrinfo* attempt = target_info;
  //start sock with error code and transverse the list trying to create a compatible socket
  for(sock = -1; attempt != NULL; attempt = attempt->ai_next){
    sock = socket(attempt->ai_family, attempt->ai_socktype, attempt->ai_protocol);
    if(sock != -1) break;
  }

  if(attempt == NULL){
    fprintf(stderr, "Error creating socket.\n");
    exit(1);
  }

  //create the request string...
  char request[MAX_FILENAME + 1];
  int request_size = 0;
  memset(request, 0, sizeof(request));

  char* ending = request;
  for(int i = 2; i < argc; ++i){
    strcpy(ending, argv[i]);
    ending += strlen(argv[i]) + 1;
    request_size += strlen(argv[i]) + 1;
  }

  if(sendto(sock, request, request_size, 0, attempt->ai_addr, attempt->ai_addrlen) == -1){
    fprintf(stderr, "Error sending handshake to server.\n");
    exit(1);
  }

  struct sockaddr_storage server;
  socklen_t server_len = sizeof(server);
  WavInfo file_info;

  if(recvfrom(sock, &file_info, sizeof(WavInfo), 0, (struct sockaddr*)&server, &server_len) == -1){
    fprintf(stderr, "Error reading file info from server.\n");
    exit(1);
  }

  int audio_device = aud_writeinit(file_info.sample_rate, file_info.sample_size, file_info.channels);
  if(audio_device == -1){
    fprintf(stderr, "Error creating audio device.\n");
    exit(1);
  }

  fd_set reader_set, writer_set;
  uint32_t seqn = 0;
  char is_in_sync = 0;
  char should_stop = 0;
  BufferQueue queue = NULL;

  do {
    FD_ZERO(&reader_set);
    FD_ZERO(&writer_set);

    FD_SET(sock, &reader_set);
    if(is_in_sync){
      FD_SET(audio_device, &writer_set);
    }
    struct timeval timeout;
    timeout.tv_usec = 1000;

    status = select(FD_SETSIZE, &reader_set, &writer_set, NULL, &timeout);

    if(status==-1){
      fprintf(stderr, "Error selecting file descriptors.\n");
      exit(1);
    }

    if(status == 0 && queue != NULL){
      seqn = queue->buffer.seqn;//fast forward
      is_in_sync = 1;
    }

    if(FD_ISSET(sock, &reader_set)){
      Buffer input_buffer;
      status = readBuffer(sock, &input_buffer);
      if(status == -1)
        continue;
      //received meaningfull packet...
      
      if(input_buffer.size != 0){
        should_stop = 0;
      }else {
        should_stop++;
        freeBuffer(&input_buffer);
        continue;
      }

      if(input_buffer.seqn < seqn){
        freeBuffer(&input_buffer);
        continue;
      }

      insertOnQueue(&input_buffer, &queue);
      is_in_sync = 1;
      continue;
    }

    if(queue != NULL && is_in_sync && FD_ISSET(audio_device, &writer_set)){
      Buffer to_write;
      popFromQueue(&queue, &to_write);
      if(write(audio_device, to_write.data, to_write.size) != to_write.size){
        fprintf(stderr, "Error writing to audio device.\n");
        exit(2);
      }
      seqn++;
      freeBuffer(&to_write);  
    }
  }while(queue != NULL || should_stop < 2);

  close(audio_device);
  close(sock);
  return 0;
}
