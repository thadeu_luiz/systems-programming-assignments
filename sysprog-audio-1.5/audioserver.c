//==============================================================================
//
//       Filename:  audioserver.c
//
//    Description:  Server...
//
//        Version:  1.0
//        Created:  10/15/2015 02:38:34 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "audio.h"
#include "libtest.h"

#define PORT "10666"
#define MAX_INCOMING_CONNECTIONS 10

int createListener(const char* port){
  struct addrinfo hint, *hostinfo;
  memset(&hint, 0, sizeof(hint));
  hint.ai_family = AF_INET6;
  hint.ai_socktype = SOCK_DGRAM;
  hint.ai_flags = AI_PASSIVE | AI_V4MAPPED;

  int status;
  if( (status = getaddrinfo(NULL, port, &hint, &hostinfo)) ){
    fprintf(stderr, "Error getting address info: %s.\n", gai_strerror(status));
    exit(1);
  }

  //create socket
  int listener;
  struct addrinfo *attempt;
  for(listener = -1, attempt = hostinfo; attempt != NULL; attempt = attempt->ai_next){
    listener = socket(attempt->ai_family, attempt->ai_socktype, attempt->ai_protocol);
    if(listener != -1) break;
  }

  //no socket found...
  if(attempt == NULL){
    fprintf(stderr, "Error creating socket.\n");
    exit(1);
  }

  //bind our first created socket...
  if(bind(listener, attempt->ai_addr, attempt->ai_addrlen) == -1){
    status = errno;
    fprintf(stderr, "Error binding socket to port %s.\nerrno %d.\n", PORT, status);
    exit(1);
  }

  //listener socket aquired and bound
  //we are free to deallocate our linked list
  freeaddrinfo(hostinfo);
  return listener;
}

int delayFactor(WavInfo info){
  double time_per_sample = 1.0/info.sample_rate;
  double samples = (double)MAX_PACKET_PAYLOAD/(info.channels * info.sample_size);
  return 0.5 * 10e6 * time_per_sample * samples;
}

int main(int argc, char* argv[]){
  int status, listener;

  if(argc != 1){
    printf("%s: Error - this program takes no args.\n", argv[0]);
    exit(1);
  }

  listener = createListener(PORT);

  pid_t pid;
  struct sockaddr_storage client;
  char client_request[MAX_FILENAME + 1];
  socklen_t client_size = sizeof(client);

  do { 
    memset(client_request, 0, sizeof(client_request));
    status = recvfrom(listener, client_request, MAX_FILENAME, 0,
        (struct sockaddr*)&client,
        &client_size);
    pid = fork();
  }while(pid != 0);

  close(listener);

  listener = socket(client.ss_family, SOCK_DGRAM, 0);

  if(listener == -1){
    printf("Error creating new socket.\n");
    exit(1);
  }

  if(status == -1){
    status = errno;
    fprintf(stderr, "Error reading handshake.\nerrno %d.\n", status);
    exit(1);
  }

  //read the requested plugins...
  int mono = 0;
  int clip = 0;
  int volume = 100;

  char* request_iterator = client_request;

  while(*request_iterator != '\0'){
    if(!strcmp("mono", request_iterator)){
      mono = 1;
    }

    if(!strcmp("c", request_iterator)){
      request_iterator += 2;
      char* end;
      clip = strtol(request_iterator, &end, 10);
    }

    if(!strcmp("v", request_iterator)){
      request_iterator += 2;
      char* end;
      volume = strtol(request_iterator, &end, 10);
    }

    request_iterator += strlen(request_iterator) + 1;
  }

  if(volume <= 0 || volume > 100)
    volume = 100;

  WavInfo file_info;
  int audio_file = aud_readinit(client_request,
      &file_info.sample_rate,
      &file_info.sample_size,
      &file_info.channels);
  if(audio_file < 0){
    //error information is printed by aud_readinit
    exit(1);
  }

  //if file is stereo but requested mono, process the file
  //else do nothing
  if(file_info.channels == 1){
    mono = 0;
  }
  else if(mono){
    file_info.channels = 1;
  }

  //mono == 1 means that we should reduce the channels...
  //we will make an average of both channels...
  status = sendto(listener, &file_info, sizeof(file_info), 0,
      (struct sockaddr*)&client, client_size);

  if(status == -1){
    fprintf(stderr, "Error sending audio file information.\n");
    exit(1);
  }

  char read_buffer[MAX_PACKET_PAYLOAD * (mono + 1)];
  int seqn = 0;

  while((status = read(audio_file, read_buffer, sizeof(read_buffer)))){
    if(status == -1){
      fprintf(stderr, "Error reading audio file.\n");
      exit(1);
    }

    if(mono){
      if(file_info.sample_size == 8){
        int i = 0;
        for(int j = 0; j < status - 2; j+=2){
          read_buffer[i++] = read_buffer[j];
        }
      } else if(file_info.sample_size == 16){
        int16_t* long_read_buffer = (int16_t*)read_buffer;
        int size = status / 2;

        int i = 0;
        for(int j = 0; j < size - 2; j+=2){
          long_read_buffer[i++] = long_read_buffer[j];
        }
      }

      status /= 2;
      //finally, correct the block size
    }

    if(clip || volume!= 100){
      int clipi;
      switch(file_info.sample_size){
        case 8:
          if(clip){
            clipi = (0xFF * clip)/100;
          } else {
            clipi = 0xFF;
          }

          for(int i = 0; i < status; ++i){
            if(abs(read_buffer[i] > clipi))
              read_buffer[i] = clipi*(read_buffer[i] > 0 ? 1 : -1);
            read_buffer[i] = ((int)read_buffer[i] * volume)/100;
          }
          break;

        case 16:
          if(clip) {
            clipi = (0xFFFF * clip)/100;
          } else {
            clipi = 0xFFFF;
          }

          int16_t* long_read_buffer = (int16_t*)read_buffer;

          for(int i = 0; i < status/2; ++i){
            if(abs(long_read_buffer[i]) > clipi)
              long_read_buffer[i] = clipi*(long_read_buffer[i] > 0 ? 1 : -1);
            long_read_buffer[i] = ((int)long_read_buffer[i] * volume)/100;
          }
          break;
      }
    }

    sendBlock(listener, client, client_size, read_buffer, status, &seqn);
    usleep(delayFactor(file_info));
  }

  for(int i = 0; i<10; ++i){
    status = sendto(listener, &seqn, sizeof(seqn), 0,
        (struct sockaddr*)&client, client_size);
    if(status == -1){
      fprintf(stderr, "Error sending eof block.\n");
      exit(1);
    }
    seqn++;
  }
  close(audio_file);

  close(listener);

  printf("Done sending file %s to client.\n", client_request);
  return 0;
}
