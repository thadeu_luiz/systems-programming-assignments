/*
 * =====================================================================================
 *
 *       Filename:  mystrcmp.c
 *
 *    Description:  My implementation of string equality function
 *
 *        Version:  1.0
 *        Created:  09/02/2015 09:53:15 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
 *
 * =====================================================================================
 */

#include <stdio.h>

/* This function tests equality on strings */
int mystrcmp(char* lhs, char* rhs){

  for(;*lhs && *rhs; ++lhs, ++rhs){ /* loop while not on \0 */
    if(*lhs != *rhs) return 0; /* difference detected */
  }
  return !(*lhs || *rhs) ? 1 : 0; /* true if both are on \0 */
}


int main(int argc, char** argv){
  const char* responses[] = { "different", "identical" };

  if(argc != 3){
    printf("Usage: %s <first string> <second string>\n", argv[0]);
    return 1;
  }

  printf("The two strings are %s.\n", responses[ mystrcmp(argv[1], argv[2]) ] );
  return 0;
}
