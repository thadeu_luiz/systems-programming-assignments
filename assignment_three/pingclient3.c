//==============================================================================
//
//       Filename:  pingclient2.c
//
//    Description:  Second version of the ping client
//
//        Version:  1.0
//        Created:  30/09/2015 23:48:11 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define PORT "10666"

void sleep_remain(struct timeval* tv){
  struct timeval now;
  long remaining_time;
  gettimeofday(&now, NULL);
  remaining_time = 1e6L - 1e6L*(now.tv_sec - tv->tv_sec) + (now.tv_usec - tv->tv_usec); 
  if(remaining_time > 0)
    usleep(remaining_time);
}

int main(int argc, char* argv[]){
  struct addrinfo hint, *target_info;
  int status, sock;
  int message = 0;

  if(argc != 2){
    printf("Usage: %s <hostname>\n", argv[0]);
    exit(1);
  }

  memset(&hint, 0, sizeof(hint));
  hint.ai_family = AF_UNSPEC;
  hint.ai_socktype = SOCK_DGRAM;

  if(getaddrinfo(argv[1], PORT, &hint, &target_info) != 0){
    status = errno;
    fprintf(stderr, "Error retrieving getting host info.\nerrno: %d.\n", status);
    exit(1);
  }

  struct addrinfo* attempt = target_info;
  //start sock with error code and transverse the list trying to create a socket
  for(sock = -1; attempt != NULL; attempt = attempt->ai_next){
    sock = socket(attempt->ai_family, attempt->ai_socktype, attempt->ai_protocol);
    if(sock != -1) break;
  }

  if(attempt == NULL){
    fprintf(stderr, "Error creating socket.\n");
    exit(1);
  }

  //ok, socket is created.
  char send = 1; //flag to prevent sending of other packets
  while(1){
    int echo;
    long time;
    struct timeval start_time, end_time, timeout;
    fd_set read_fd_set;

    timeout.tv_sec = 1;
    timeout.tv_usec = 0;
    FD_ZERO(&read_fd_set);
    FD_SET(sock, &read_fd_set);

    if(send){
      message+=1;
      status = sendto(sock, &message, sizeof(message), 0, attempt->ai_addr, attempt->ai_addrlen);
      gettimeofday(&start_time, NULL);
      if(status < (long)sizeof(message)){
        fprintf(stderr, "Error sending packet %d to server.\n", message);
        sleep_remain(&start_time);
        continue;
      }
    }
    else
      send = 1; //next time we should send a packet...

    if(select(sock+1, &read_fd_set, NULL, NULL, &timeout) == 0){
      printf("Packet %d timed out.\n", message);
      continue;//no need to wait
    }

    status = recvfrom(sock, &echo, sizeof(echo), 0, NULL, NULL);
    gettimeofday(&end_time, NULL);

    if(status < (long)sizeof(echo)){
      fprintf(stderr, "Error retrieving ping response.\n");
      sleep_remain(&start_time);
      continue;
    }

    if(message != echo){
      printf("Echoed message does not match original message.\n");
      send = 0;//prevent increase of the counter and resetting of starttime
      continue;
    }

    time = 1e6l*(end_time.tv_sec - start_time.tv_sec) + (end_time.tv_usec - start_time.tv_usec);

    printf("Packet %d: %li microseconds\n",message, time);
    sleep_remain(&start_time);
  }
}
