/*
 * =====================================================================================
 *
 *       Filename:  mystrlen.c
 *
 *    Description:  My own implementation of strlen
 *
 *        Version:  1.0
 *        Created:  09/02/2015 08:25:41 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
 *
 * =====================================================================================
 */

#include <stdio.h>

/* This function returns the length of the input string */
int mystrlen(char* input_string) {
  int length = 0;
  while( *input_string++ ){ /* keep looping till we hit \0 */
    ++length;
  }
  return length;
}

int main(int argc, char** argv) {
  if (argc!=2) {
    printf("Usage: %s <input_string_with_no_space_inside_it>\n", argv[0]);
    return 1;
  }

  printf("The length is: %d characters\n", mystrlen(argv[1]) );
  return 0;
}
