/*
 * =====================================================================================
 *
 *       Filename:  reverse.c
 *
 *    Description:  File reversal program
 *
 *        Version:  1.0
 *        Created:  09/02/2015 10:26:02 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXBUF 1024

typedef struct BufferStruct {
  int size; /* position of last used memory */
  char* chars;
} Buffer;

void check_alloc(void* ptr){
  if(ptr != NULL) return;
  fprintf(stderr, "Error: bad allocation -- Terminating\n");
  exit(EXIT_FAILURE);
}

Buffer fill_buffer(FILE* file){
  Buffer buf;
  char static_buffer[MAXBUF];
  buf.size = fread(static_buffer, sizeof(char), MAXBUF, file);
  buf.chars = malloc(buf.size * sizeof(char));
  check_alloc(buf.chars);
  memcpy(buf.chars, static_buffer, buf.size * sizeof(char));
  return buf;
}

void print_buffer(const Buffer* buff, FILE* file){
  fwrite(buff->chars, sizeof(char), buff->size, file);
}

void destroy_buffer(Buffer* buf){
  free(buf->chars);
  buf->size = 0;
  buf->chars = NULL;
}

void reverse_buffer(Buffer* buff){
  int forward, reverse;
  char tmp;
  if(buff->size == 0) return; /* nothing to do here */
  for(forward = 0, reverse = buff->size - 1;
      forward < reverse;
      ++forward, --reverse){
    tmp = buff->chars[forward];
    buff->chars[forward] = buff->chars[reverse];
    buff->chars[reverse] = tmp;
  }
}

typedef struct BufferVectorStruct{
  int max_size;
  int size;
  Buffer* buffers;
} BufferVector;

void enlarge_vector(BufferVector* vec){
  Buffer* new_buffers;
  if(vec->max_size == 0){
    vec->max_size = 1;
    vec->buffers = malloc(sizeof(Buffer));
    check_alloc(vec->buffers);
    return;
  }
  vec->max_size*=2;
  new_buffers = malloc(vec->max_size * sizeof(Buffer));
  check_alloc(new_buffers);
  memcpy(new_buffers, vec->buffers, vec->size * sizeof(Buffer));
  free(vec->buffers);
  vec->buffers = new_buffers;
}

BufferVector create_vector(int sz){
  BufferVector vec;
  vec.max_size = sz;
  vec.size = 0;
  vec.buffers = (sz == 0) ? NULL : malloc(sz * sizeof(Buffer));
  if(sz != 0){
    check_alloc(vec.buffers);
  }
  return vec;
}

void destroy_vector(BufferVector* vec){
  int i;
  for(i = 0; i < vec->size; ++i){
    destroy_buffer(&vec->buffers[i]);
  }
  free(vec->buffers);
  vec->buffers = NULL;
  vec->max_size = 0;
  vec->size = 0;
}

/* back inserter */
void insert_buffer(BufferVector* vec, Buffer buf){
  if(vec->max_size == vec->size){
    enlarge_vector(vec);
  }
  vec->buffers[vec->size] = buf;
  vec->size = vec->size + 1;
}

int main(int argc, char** argv){
  FILE* file;
  BufferVector vec;
  int i;
  if(argc != 2){
    printf("Usage: %s <file name>\n", argv[0]);
    return 1;
  }
  file = fopen(argv[1], "rb");
  if(file == NULL){
    printf("Error: could not open file %s\n", argv[1]);
    return 2;
  }

  vec = create_vector(0);

  while(!feof(file))
    insert_buffer(&vec, fill_buffer(file) );

  for(i = vec.size - 1; i >=0 ; --i){
    reverse_buffer(&vec.buffers[i]);
    print_buffer(&vec.buffers[i], stdout);
  }

  fclose(file);
  destroy_vector(&vec);

  return 0;
}
