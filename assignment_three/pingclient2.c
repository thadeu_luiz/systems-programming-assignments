//==============================================================================
//
//       Filename:  pingclient2.c
//
//    Description:  Second version of the ping client
//
//        Version:  1.0
//        Created:  30/09/2015 23:48:11 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define PORT "10666"

int main(int argc, char* argv[]){
  struct addrinfo hint, *target_info, *attempt;
  struct timeval start_time, end_time, timeout;
  int status, sock;
  long time;
  char message[] = "ping\n";
  char echo[sizeof(message)];
  fd_set read_fd_set;

  if(argc != 2){
    printf("Usage: %s <hostname>\n", argv[0]);
    exit(1);
  }

  memset(&hint, 0, sizeof(hint));
  hint.ai_family = AF_UNSPEC;
  hint.ai_socktype = SOCK_DGRAM;

  if(getaddrinfo(argv[1], PORT, &hint, &target_info) != 0){
    status = errno;
    fprintf(stderr, "Error retrieving getting host info.\nerrno: %d.\n", status);
    exit(1);
  }

  for(sock = -1, attempt = target_info; attempt != NULL; attempt = attempt->ai_next){
    sock = socket(attempt->ai_family, attempt->ai_socktype, attempt->ai_protocol);
    if(sock != -1) break;
  }

  if(attempt == NULL){
    fprintf(stderr, "Error creating socket.\n");
    exit(1);
  }

  //ok, socket is created.
  timeout.tv_sec = 1;
  timeout.tv_usec = 0;
  FD_ZERO(&read_fd_set);
  FD_SET(sock, &read_fd_set);

  status = sendto(sock, message, sizeof(message), 0, attempt->ai_addr, attempt->ai_addrlen);
  gettimeofday(&start_time, NULL);
  if(status < (long)sizeof(message)){
    fprintf(stderr, "Error sending message to server.\n");
    exit(1);
  }

  if(select(sock+1, &read_fd_set, NULL, NULL, &timeout) == 0){
    printf("Packet timed out.\n");
    close(sock);
    return 0;
  }

  status = recvfrom(sock, echo, sizeof(echo), 0, NULL, NULL);
  gettimeofday(&end_time, NULL);

  if(status < (long)sizeof(echo)){
    fprintf(stderr, "Error retrieving ping response.\n");
    exit(1);
  }

  close(sock);

  if(strcmp(message, echo) != 0){
    printf("Echoed message does not match original message.\n");
    return 0;
  }
  time = 1e6l*(end_time.tv_sec-start_time.tv_sec) + (end_time.tv_usec - start_time.tv_usec);

  printf("Packet 1: %li microseconds\n", time);
  return 0;
}
